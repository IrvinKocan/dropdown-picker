//
//  DropDownPicker.swift

import UIKit

enum DropDownPickerType {
    case orderType
    case orderFrequency
    case gender
    
    var title: String {
        switch self {
        case .orderType:
            return "order_type".localized
        case .orderFrequency:
            return "frequency".localized
        case .gender:
            return "gender_title".localized
        }
    }
    
    var data: [DropDownPickerValue] {
        switch self {
        case .orderType:
            return [.executeImmediately, .simpleOrder, .standingOrder]
        case .orderFrequency:
            return [.weekly, .twoWeeks, .monthly, .annually]
        case .gender:
            return [.mr, .mrs]
        }
    }
}

enum DropDownPickerValue {
    case executeImmediately
    case simpleOrder
    case standingOrder
    case weekly
    case twoWeeks
    case monthly
    case annually
    case mr
    case mrs
    
    var title: String {
        switch self {
        case .executeImmediately:
            return "order_execute_immediately".localized
        case .simpleOrder:
            return "order_simple".localized
        case .standingOrder:
            return "order_standing".localized
        case .weekly:
            return "weekly".localized
        case .twoWeeks:
            return "two_weeks".localized
        case .monthly:
            return "monthly".localized
        case.annually:
            return "annually".localized
        case .mr:
            return "mr_title".localized
        case .mrs:
            return "mrs_title".localized
        }
    }
    
    var payloadValue: String {
        switch self {
        case .mr:
            return "M"
        case .mrs:
            return "F"
        default:
            return ""
        }
    }
    
    init(json: String) {
        switch json {
        case "M": self = .mr
        case "F": self = .mrs
        default: self = .executeImmediately
        }
    }
}

class DropDownPickerViewModel {
    var title: String
    var value: DropDownPickerValue?
    var data: [DropDownPickerValue]
    
    init(title: String, value: DropDownPickerValue?, data: [DropDownPickerValue]) {
        self.title = title
        self.value = value
        self.data = data
    }
    
    static func orderTypePicker(value: DropDownPickerValue = DropDownPickerType.orderType.data[0]) -> DropDownPickerViewModel {
        return DropDownPickerViewModel(title: DropDownPickerType.orderType.title, value: value, data: DropDownPickerType.orderType.data)
    }
    
    static func orderFrequencyPicker(value: DropDownPickerValue = DropDownPickerType.orderFrequency.data[0]) -> DropDownPickerViewModel {
        return DropDownPickerViewModel(title: DropDownPickerType.orderFrequency.title, value: value, data: DropDownPickerType.orderFrequency.data)
    }
    
    static func createPicker(_ title: String, value: String) -> DropDownPickerViewModel {
        switch title {
        case RegistrationFieldName.gender.rawValue:
            return DropDownPickerViewModel(title: DropDownPickerType.gender.title, value: nil, data: DropDownPickerType.gender.data)
        default:
            return DropDownPickerViewModel(title: DropDownPickerType.gender.title, value: nil, data: DropDownPickerType.gender.data)
        }
    }
}

let pickCell = "PickCell"

protocol DropDownPickerDelegate: class {
    func didSelect(value: DropDownPickerValue)
}

class DropDownPicker: UIView, UITableViewDataSource, UITableViewDelegate {
    
    weak var delegate: DropDownPickerDelegate?
    
    var border: CALayer = {
        let border = CALayer()
        let width = CGFloat(1)
        border.borderWidth = width
        border.borderColor = UIColor.darkGray.cgColor
        return border
    }()
    
    var pickerModel: DropDownPickerViewModel = DropDownPickerViewModel.orderTypePicker() {
        didSet {
            if pickerModel.value == nil {
                title.isHidden = true
                label.textColor = Branding.Color.dropDownHeaderTitle
            } else {
                title.isHidden = false
                label.textColor = Branding.Color.tint
            }
            
            title.text = pickerModel.title
            label.text = pickerModel.value?.title ?? pickerModel.title
            picker.reloadData()
        }
    }
    
    lazy var title: UILabel = {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Branding.Color.dropDownHeaderTitle
        label.text = self.pickerModel.title
        label.font = Branding.Font.dropDownHeader
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showPicker)))
        return label
    }()
    
    lazy var label: UILabel = {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = Branding.Color.tint
        label.text = self.pickerModel.data[0].title
        label.font = Branding.Font.menuRowTitle
        label.numberOfLines = 1
        return label
    }()
    
    lazy var icon: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "Drop_Down"))
        imageView.heightAnchor.constraint(equalToConstant: 16).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 16).isActive = true
        return imageView
    }()
    
    lazy var emptySpace: UIView = {
        let empty = UIView()
        empty.backgroundColor = .clear
        empty.widthAnchor.constraint(equalToConstant: 5).isActive = true
        empty.heightAnchor.constraint(equalToConstant: 20).isActive = true
        return empty
    }()
    
    lazy var horizontalStack: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [self.label, self.icon, self.emptySpace])
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.alignment = .center
        stack.isUserInteractionEnabled = true
        stack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showPicker)))
        return stack
    }()
    
    lazy var picker: IntrinsicTableView = {
        let table = IntrinsicTableView(frame: .zero)
        table.isHidden = true
        table.register(UINib(nibName: pickCell, bundle: nil), forCellReuseIdentifier: pickCell)
        table.estimatedRowHeight = 50
        table.rowHeight = UITableViewAutomaticDimension
        table.dataSource = self
        table.delegate = self
        table.layer.masksToBounds = true
        return table
    }()
    
    lazy var stack: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [self.title, self.horizontalStack, self.picker])
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .fill
        stack.spacing = 5
        return stack
    }()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pickerModel.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: pickCell, for: indexPath) as! PickCell
        cell.pickLabel.text = pickerModel.data[indexPath.row].title
        
        cell.didSelectClosure = { [weak self] in
            self?.optionPicked(row: indexPath.row)
        }
        
        guard let value = pickerModel.value else {
            cell.bindUnselected()
            return cell
        }
        let selectedRow = pickerModel.data.index(of: value)
        if indexPath.row == selectedRow {
            cell.bindSelected()
        } else {
            cell.bindUnselected()
        }
        return cell
    }
    
    func optionPicked(row: Int) {
        if pickerModel.value != pickerModel.data[row] {
            delegate?.didSelect(value: pickerModel.data[row])
        }
        pickerModel.value = pickerModel.data[row]
        label.text = pickerModel.value?.title
        hidePicker()
        updatePlaceholder()
    }
    
    func updatePlaceholder() {
        if pickerModel.value != nil {
            title.isHidden = false
            label.textColor = Branding.Color.tint
        }
    }
    
    func reset() {
        pickerModel.value = pickerModel.data[0]
        label.text = pickerModel.value?.title
        delegate?.didSelect(value: pickerModel.value!)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonSetup()
    }
    
    func commonSetup() {
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(stack)
        stack.fit(to: self)
    }
    
    var borderAdded = false
    
    override func layoutSubviews() {
        addBottomBorder()
    }
    
    func addBottomBorder() {
        if !borderAdded {
            border.frame = CGRect(x: 0, y: horizontalStack.frame.size.height + 5, width: horizontalStack.frame.size.width, height: 1)
            
            if border.superlayer != nil {
                border.removeFromSuperlayer()
            }
            horizontalStack.layer.addSublayer(border)
            picker.cornerBorder()
            picker.separatorStyle = .none
        }
    }
    
    @objc func showPicker() {
        if picker.isHidden {
            picker.reloadData()
            picker.isHidden = false
            icon.image = #imageLiteral(resourceName: "Drop_Up")
        } else {
            hidePicker()
        }
    }
    
    func hidePicker() {
        picker.isHidden = true
        borderAdded = true
        icon.image = #imageLiteral(resourceName: "Drop_Down")
    }
}

extension UIView {
    func cornerBorder() {
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: [.bottomLeft, .bottomRight],
                                    cornerRadii: CGSize(width: 10.5, height: 10.5))
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        layer.mask = maskLayer
        
        let borderPath = UIBezierPath(roundedRect: bounds,
                                      byRoundingCorners: [.bottomLeft, .bottomRight],
                                      cornerRadii: CGSize(width: 10, height: 10))
        let borderLayer = CAShapeLayer()
        borderLayer.frame = bounds
        borderLayer.path = borderPath.cgPath
        borderLayer.strokeColor = UIColor.darkGray.cgColor
        borderLayer.lineWidth = 2
        borderLayer.fillColor = nil
        layer.addSublayer(borderLayer)
    }
}

class IntrinsicTableView: UITableView {
    
    override var contentSize: CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }    
    
    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIViewNoIntrinsicMetric, height: contentSize.height)
    }
    
}

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
