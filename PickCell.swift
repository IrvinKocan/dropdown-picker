//
//  PickCell.swift

import UIKit

class PickCell: UITableViewCell {

    @IBOutlet weak var pickLabel: UILabel!
    var didSelectClosure: (()-> Void)?
    var gestureRecognizer: UITapGestureRecognizer?
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            fillStyle()
        }
    }
    
    func bindSelected() {
        fillStyle()
        addGestureRecognizer()
    }
    
    func bindUnselected() {
        emptyStyle()
        addGestureRecognizer()
    }
    
    func addGestureRecognizer() {
        if gestureRecognizer == nil {
            gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didSelect))
            contentView.addGestureRecognizer(gestureRecognizer!)
        }
    }
    
    @objc func didSelect() {
        guard let closure = didSelectClosure else {
            return
        }
        closure()
    }
    
    func fillStyle() {
        backgroundColor = Branding.Color.tint
        pickLabel.textColor = Branding.Color.dropDownSelectedValue
    }
    
    func emptyStyle() {
        backgroundColor = Branding.Color.dropDownSelectedValue
        pickLabel.textColor = Branding.Color.dropDownHeaderTitle
    }
}
